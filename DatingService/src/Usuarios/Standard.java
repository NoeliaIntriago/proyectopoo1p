/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Usuarios;

/**
 *
 * @author Moreira Guillermo
 */
import Mensajeria.*;
import java.util.ArrayList;
/**
 * 
 * @author Noelia Intriago: esta clase define la creación de usuarios tipo Standard
 */
public class Standard extends Usuario{

    /**
     *
     * @param email
     * @param nombre
     * @param fechaNacimiento
     * @param contrasena
     * @param informacion
     * @param mensajes
     */
    public Standard(String email, String nombre, String fechaNacimiento, String contrasena, ArrayList<String> informacion, ArrayList<Mensaje> mensajes){
        super(email, nombre, fechaNacimiento, contrasena, informacion, mensajes);  
    } 
}


       

