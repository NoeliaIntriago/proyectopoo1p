/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Usuarios;


import java.util.ArrayList;
import Mensajeria.*;

/**
 *
 * @author Moreira Guillermo
 * Esta clase define la creacion de usuarios con cuenta tipo premium
 */
public class Premium extends Usuario{
    private String numeroTarjeta;
    private String caducidadTarjeta;
    
    public Premium(String email, String nombre, String fechaNacimiento, String contrasena, ArrayList<String> informacion, ArrayList<Mensaje> mensajes, String numeroTarjeta, String caducidadTarjeta){
        super(email, nombre, fechaNacimiento, contrasena, informacion, mensajes);
        this.numeroTarjeta = numeroTarjeta;
        this.caducidadTarjeta = caducidadTarjeta;
    }
    
    public void darSuperLike(){
        System.out.println("Dio un super like");
    }
    
    public String imprimirSuperLike(){
        return this.nombre + " te dio un súper like!";
    }
}