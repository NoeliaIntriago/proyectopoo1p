/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Usuarios;

import Mensajeria.*;
import java.io.*;
import java.util.ArrayList;
/**
 *
 * @author Moreira Guillermo  
 * Esta clase permite la creacion de nuevos usuarios
 */
public class Usuario implements Serializable {
    protected String email;
    protected String nombre;
    protected String fechaNacimiento;
    protected String contrasena;
    protected ArrayList<String> informacion;
    protected ArrayList<Mensaje> mensajes;
    
    public Usuario(String email, String nombre, String fechaNacimiento, String contrasena, ArrayList<String> informacion, ArrayList<Mensaje> mensajes){
        this.email = email;
        this.nombre = nombre;
        this.fechaNacimiento = fechaNacimiento;
        this.contrasena = contrasena;
        this.informacion = informacion;
        this.mensajes = mensajes;
    }
    
    public String getEmail(){
        return email;
    }

    public String getNombre() {
        return nombre;
    }
      
    public String getFechaNacimiento(){
        return fechaNacimiento;
    }

    public String getContrasena(){
        return contrasena;
    } 
    
    public ArrayList<String> getInformacion(){
        return informacion;
    } 
    
    public ArrayList<Mensaje> getMensajes(){
        return mensajes;
    }
    
    public void enviarMensaje(Usuario destino, Mensaje mensaje){ 
        mensaje = new Mensaje(mensaje.getEmisor(),mensaje.getTexto());
        destino.mensajes.add(mensaje);
    }
}
