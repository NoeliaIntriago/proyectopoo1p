/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Usuarios;

/**
 *
 * @author Noelia Intriago
 * Esta clase contiene las posibles elecciones de hobbies
 */
public enum Intereses {
    Lectura,
    Música,
    Cocina,
    Fotografía,
    Deportes,
    Baile,
    Escritura,
    Pintura,
    Viajar,
    Naturaleza,
    Cine,
    Animales,
    Jardineria,
    Moda
}
