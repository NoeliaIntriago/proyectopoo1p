/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Usuarios;

/**
 *
 * @author Noelia Intriago
 * Esta clase contiene las posibles elecciones de profesiones
 */

public enum Profesiones {
    Electricista,	
    Secretaria,
    Antropólogo,
    Administrador,
    Contador,	
    Psicoanalista,
    Arqueólogo,
    Enfermero,	
    Paramédico,
    Geógrafo,	
    Músico,
    Psicólogo,	
    Computista,	
    Economista,
    Farmacólogo,	
    Ecólogo
}
