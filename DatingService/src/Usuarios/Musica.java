/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Usuarios;

/**
 *
 * @author Noelia Intriago
 * Esta clase contiene las posibles elecciones de géneros musicales
 */
public enum Musica {
    Rock,
    Bachata,
    Pop,
    Electrónica,
    Cumbia,
    Salsa,
    Reggaeton,
    Blues,
    Indie
}
