/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datingservice;

import Interfaces.Buzon;
import Interfaces.InterfazUsuario;
import static Interfaces.InterfazUsuario.usuarios;
import Interfaces.PerfilUsuario;
import Interfaces.Sesion;
import Usuarios.Usuario;
import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Noelia Intriago
 */
public class DatingService {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args){
        deserializar();
        Scanner entrada = new Scanner(System.in);
        
        System.out.println("Bienvenido a la aplicación de citas\n1. Crear cuenta usuario\n2. Iniciar sesión\n3. Salir");
        System.out.print("Ingrese opción: ");
        String opcion = entrada.nextLine();
        while(!"1".equals(opcion) && !"2".equals(opcion) && !"3".equals(opcion)){
            System.out.println("Ingrese dato correcto.");
            System.out.print("Ingrese opción: ");
            opcion = entrada.nextLine(); 
        }
        boolean working = true;
        
        while(working){
            switch (opcion) {
                case "1":
                    InterfazUsuario.iniciar();
                    System.out.println("\nBienvenido a la aplicación de citas\n1. Crear cuenta usuario\n2. Iniciar sesión\n3. Salir");
                    System.out.print("Ingrese opción: ");
                    opcion = entrada.nextLine();
                    break;
                case "2":
                    Sesion.iniciarSesion(usuarios);
                    if(Sesion.usuarioEscogido!=null){
                        System.out.println("\nBienvenido!\n1. Completar el perfil\n2. Buscar pareja\n3. Buzón("+ Sesion.usuarioEscogido.getMensajes().size() +" mensajes)\n4. Cerrar sesion");
                        System.out.print("Ingrese opción: ");
                        String menu = entrada.nextLine();
                        while(!"1".equals(menu) && !"2".equals(menu) && !"3".equals(menu) && !"4".equals(menu)){
                            System.out.println("Ingrese dato correcto.");
                            System.out.print("Ingrese opción: ");
                            menu = entrada.nextLine(); 
                        }
                        boolean selecting = true;
                        while(selecting) {
                            switch(menu){
                                case "1":
                                    PerfilUsuario.completarPerfil(usuarios);
                                    System.out.println("\n1. Completar el perfil\n2. Buscar pareja\n3. Buzón("+ Sesion.usuarioEscogido.getMensajes().size() +" mensajes)\n4. Cerrar sesión\n");
                                    System.out.print("Ingrese opción: ");
                                    menu = entrada.nextLine();
                                    break;
                                case "2":
                                    PerfilUsuario.buscarPareja(usuarios);
                                    System.out.println("\n1. Completar el perfil\n2. Buscar pareja\n3. Buzón("+ Sesion.usuarioEscogido.getMensajes().size() +" mensajes)\n4. Cerrar sesión\n");
                                    System.out.print("Ingrese opción: ");
                                    menu = entrada.nextLine();
                                    break;
                                case "3":
                                    Buzon.mostrarBuzon(Sesion.usuarioEscogido.getMensajes());
                                    System.out.println("\n1. Completar el perfil\n2. Buscar pareja\n3. Buzón("+ Sesion.usuarioEscogido.getMensajes().size() +" mensajes)\n4. Cerrar sesión\n");                                    
                                    System.out.print("Ingrese opción: ");
                                    menu = entrada.nextLine();
                                    break;
                                case "4":
                                    System.out.println("Ha cerrado sesión exitosamente.\n");
                                    selecting = false;
                                    break;
                            }
                        }
                    }
                    System.out.println("\nBienvenido a la aplicación de citas\n1. Crear cuenta usuario\n2. Iniciar sesión\n3. Salir");
                    System.out.print("Ingrese opción: ");
                    opcion = entrada.nextLine();
                    break;
                case "3":
                    System.out.println("Gracias por usar el programa.");
                    working = false;
                    break;
                default:
                    System.out.println("Opción no soportada.");
                    System.out.print("Ingrese opción: ");
                    opcion = entrada.nextLine();
                    break;
            }
        }
        serializar();
    }
    
    public static void serializar(){
        try(ObjectOutputStream serializer = new ObjectOutputStream(new FileOutputStream("src/Archivos/UsuariosRegistrados.txt"))){
            serializer.writeObject(usuarios);
            serializer.close();
        }
        catch(IOException e){
            System.out.print("Error en serialización de usuarios: ");
            System.err.println(e);
        }
    }
    
    public static void deserializar(){
        try(ObjectInputStream deserializer = new ObjectInputStream(new FileInputStream("src/Archivos/UsuariosRegistrados.txt"))){
            usuarios = (ArrayList<Usuario>) deserializer.readObject();      
        }
        catch(IOException|ClassNotFoundException ex){
            System.out.println("Error en deserialización: ");
            System.err.println(ex);
        }
            
    }
}