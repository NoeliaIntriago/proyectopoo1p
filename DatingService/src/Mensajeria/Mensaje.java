/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Mensajeria;

import Usuarios.*;
import java.io.Serializable;
/**
 *
 * @author Noelia Intriago
 * Esta clase genera el mensaje recibiendo el emisor y el mensaje
 */
public class Mensaje implements Serializable{
    private Usuario emisor;
    private String texto;
    
    public Mensaje(Usuario emisor, String texto){
        this.emisor = emisor;
        this.texto = texto;
    }
    
    public Usuario getEmisor(){
        return emisor;
    }
    
    public String getTexto(){
        return texto;
    }
    @Override
    public String toString(){
        return "\nDe: "+ emisor.getNombre() +"\n"+this.texto+"\nMensaje del sistema:\nContacta con "+emisor.getNombre()+" al correo: "+emisor.getEmail();
    }
}
