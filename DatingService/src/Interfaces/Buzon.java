/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaces;

import Mensajeria.Mensaje;
import java.util.ArrayList;
import java.util.Scanner;
/**
 *
 * @author Isabella Prado
 * Esta clase crea la interfaz del buzón de un usuario
 */
public class Buzon {
    /**
     * 
     * Este método muestra los mensajes que se encuentran en el buzón de un usuario.
     * @param mensajes
     */
    public static void mostrarBuzon(ArrayList<Mensaje> mensajes){
        int cantMensajes = mensajes.size();
        Scanner entrada = new Scanner(System.in);
        if(mensajes.isEmpty()){
            System.out.println("No existen mensajes para ver.");
        }else{
            System.out.println("Tiene "+cantMensajes+" mensajes");
            for(int m=0;m<mensajes.size();m++){
                System.out.println((m+1)+". "+mensajes.get(m).getEmisor().getNombre());
            }
            System.out.print("Cuál deseas leer?: ");
            int mensaje = entrada.nextInt();
            while(mensaje < 0 || mensaje > mensajes.size()){
                System.out.println("Ingrese dato correcto.");
                System.out.print("Cuál deseas leer?: ");
                mensaje = entrada.nextInt();
            }
            System.out.println(mensajes.get(mensaje-1).toString());
        }
    }
}
