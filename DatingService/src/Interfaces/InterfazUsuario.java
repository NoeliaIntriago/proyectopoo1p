/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaces;

import Usuarios.*;
import Mensajeria.*;
import java.util.Scanner;
import java.util.ArrayList;
/**
 *
 * @author Noelia Intriago
 */


public class InterfazUsuario {
    public static ArrayList<Usuario> usuarios = new ArrayList<>();
    /**
     * 
     * Este método crea un usuario por primera vez
     */
    public static void iniciar(){
        System.out.println("Bienvenido! Por favor, cree su usuario.\n");
        System.out.println("Seleccione el tipo de usuario: \n1. Tipo Standard\n2. Tipo Premium\n");
        
        Scanner opcion = new Scanner(System.in);
        System.out.print("Ingrese tipo: ");
        String tipo = opcion.nextLine();
        while(!"1".equals(tipo) && !"2".equals(tipo)){
            System.out.println("Ingrese dato correcto");
            System.out.print("Ingrese tipo: ");
            tipo = opcion.nextLine();
        }
        switch(tipo){
            case "1": System.out.println("Usuario tipo Standard");
                    crearUsuarioStandard();
                     System.out.println("Usuario creado exitosamente!\n");
                    break;
            case "2": System.out.println("Usuario tipo Premium");
                    crearUsuarioPremium();
                    System.out.println("Usuario creado exitosamente!\n");
                    break;
        }
    }
    /**
     * 
     * Este método crea un usuario Standard y lo agrega al arreglo de usuarios
     */
    public static void crearUsuarioStandard(){
        Scanner entrada = new Scanner(System.in);
        ArrayList<String> informacion = new ArrayList<>();
        ArrayList<Mensaje> mensajes = new ArrayList<>();
        
        System.out.println("\nCorreo electrónico");
        System.out.print("Ingrese su email: ");
        String email = entrada.nextLine();
        while (!email.endsWith("@hotmail.com") && !email.endsWith("@gmail.com") && !email.endsWith("@outlook.com") && !email.endsWith("@yahoo.com")){
            System.out.println("Ingrese formato correcto.");
            System.out.print("Ingrese su email: ");
            email = entrada.nextLine();
        }
        
        System.out.println("\nNombre");
        System.out.print("Ingrese su nombre: ");
        String nombre = entrada.nextLine();
        while(nombre.isBlank()){
            System.out.println("Ingrese formato correcto.");
            System.out.print("Ingrese su nombre: ");
            nombre = entrada.nextLine();   
        }
        
        System.out.println("\nFecha de nacimiento");
        System.out.print("Ingrese día: ");
        int dia = entrada.nextInt();
        while(dia < 0 || dia > 31){
            System.out.print("Ingrese dato correcto: ");
            dia = entrada.nextInt();
        }
        System.out.print("Ingrese mes: ");
        int mes = entrada.nextInt();
        while(mes < 0 || mes > 12){
            System.out.print("Ingrese dato correcto: ");
            mes = entrada.nextInt();
        }
        System.out.print("Ingrese año: ");
        int anio = entrada.nextInt();
        while(anio < 1920 || anio > 2002){
            System.out.print("Ingrese dato correcto: ");
            anio = entrada.nextInt();
        }
        String fechaNacimiento = dia+"/"+mes+"/"+anio;
        System.out.println("Fecha de nacimiento: "+fechaNacimiento);
        entrada.nextLine();
        
        System.out.println("\nContraseña");
        System.out.print("Ingrese su contraseña: ");
        String contrasena = entrada.nextLine();
        while(contrasena.isBlank()){
            System.out.println("Ingrese formato correcto.");
            System.out.print("Ingrese contrasena: ");
            contrasena = entrada.nextLine();   
        }
        
        Standard standardUser = new Standard(email, nombre, fechaNacimiento, contrasena, informacion, mensajes);
        
        usuarios.add(standardUser);   
    }
    
    /**
     * 
     * Este método crea un usuario Premium y lo agrega al arreglo de usuarios
     */
    public static void crearUsuarioPremium(){
        Scanner entrada = new Scanner(System.in);
        ArrayList<String> informacion = new ArrayList<>();
        ArrayList<Mensaje> mensajes  = new ArrayList<>();
        
        System.out.println("\nCorreo electrónico");
        System.out.print("Ingrese su email: ");
        String email = entrada.nextLine();
        while (!email.endsWith("@hotmail.com") && !email.endsWith("@gmail.com") && !email.endsWith("@outlook.com") && !email.endsWith("@yahoo.com")){
            System.out.println("Ingrese formato correcto.");
            System.out.print("Ingrese su email: ");
            email = entrada.nextLine();
        }
        
        System.out.println("\nNombre");
        System.out.print("Ingrese su nombre: ");
        String nombre = entrada.nextLine();
        while(nombre.isBlank()){
            System.out.println("Ingrese formato correcto.");
            System.out.print("Ingrese su nombre: ");
            nombre = entrada.nextLine();   
        }
        
        System.out.println("\nFecha de nacimiento");
        System.out.print("Ingrese día: ");
        int dia = entrada.nextInt();
        while(dia < 0 || dia > 31){
            System.out.print("Ingrese dato correcto: ");
            dia = entrada.nextInt();
        }
        System.out.print("Ingrese mes: ");
        int mes = entrada.nextInt();
        while(mes < 0 || mes > 12){
            System.out.print("Ingrese dato correcto: ");
            mes = entrada.nextInt();
        }
        System.out.print("Ingrese año: ");
        int anio = entrada.nextInt();
        while(anio < 1920 || anio > 2002){
            System.out.print("Ingrese dato correcto: ");
            anio = entrada.nextInt();
        }
        String fechaNacimiento = dia+"/"+mes+"/"+anio;
        System.out.println("Fecha de nacimiento: "+fechaNacimiento);
        entrada.nextLine();
       
        System.out.println("\nContraseña");
        System.out.print("Ingrese su contraseña: ");
        String contrasena = entrada.nextLine();
        while(contrasena.isBlank()){
            System.out.println("Ingrese formato correcto.");
            System.out.print("Ingrese contrasena: ");
            contrasena = entrada.nextLine();   
        }
        
        System.out.println("\nNúmero de tarjeta");
        System.out.print("Ingrese su número de tarjeta: ");
        String numeroTarjeta = entrada.nextLine();
        while(numeroTarjeta.length()!=16){
            System.out.println("Número de tarjeta incorrecto.");
            System.out.print("Ingrese su número de tarjeta: ");
            numeroTarjeta = entrada.nextLine();
        }
        
        System.out.println("\nFecha de caducidad de tarjeta");
        System.out.print("Ingrese mes: ");
        int mesT = entrada.nextInt();
        while(mesT < 0 || mesT > 12){
            System.out.print("Ingrese dato correcto: ");
            mesT = entrada.nextInt();
        }
        System.out.print("Ingrese año: ");
        int anioT = entrada.nextInt();
        while(anioT < 2022){
            System.out.print("Ingrese dato correcto: ");
            anioT = entrada.nextInt();
        }
        String fechaCaducidad = mesT+"/"+anioT;
        System.out.println("Fecha de caducidad de su tarjeta: "+fechaCaducidad);
        
        Premium premiumUser = new Premium(email, nombre, fechaNacimiento, contrasena, informacion, mensajes, numeroTarjeta, fechaCaducidad);
       
        usuarios.add(premiumUser);
    }
    
}
