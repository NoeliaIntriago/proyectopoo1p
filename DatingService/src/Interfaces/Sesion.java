/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaces;

import Usuarios.Usuario;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Guillermo Moreira
 * Esta clase permite al usuario iniciar y cerrar sesion.
 */
public class Sesion {
    public static Usuario usuarioEscogido;
    
    public static void iniciarSesion(ArrayList<Usuario> usuarios){
        Scanner entrada = new Scanner(System.in);
        System.out.print("Ingrese correo: ");
        String email = entrada.nextLine();
        System.out.print("Ingrese contraseña: ");
        String contrasena = entrada.nextLine();
        int c = 0;
        for (int i=0; i < usuarios.size(); i++){
            if(usuarios.get(i).getEmail().equals(email) && usuarios.get(i).getContrasena().equals(contrasena)){
                usuarioEscogido = usuarios.get(i);
                break;
            }else if(usuarios.get(i).getEmail().equals(email) && !usuarios.get(i).getContrasena().equals(contrasena)){
                System.out.println("Contraseña incorrecta, por favor verificar.");
                usuarioEscogido = null;
            }else{
                c++;
            }
        }
        if(c == usuarios.size() || usuarios.isEmpty()){
            System.out.println("Datos inexistentes, por favor regístrese.");
            usuarioEscogido = null;
        }
    }  
}
