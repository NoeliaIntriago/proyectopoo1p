/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaces;

import Mensajeria.*;
import java.util.ArrayList;
import Usuarios.*;
import static java.lang.String.valueOf;
import java.util.Scanner;

/**
 *
 * @author Noelia Intriago
 */
    
public class PerfilUsuario {
    /**
     * 
     * @param usuarios: arreglo de usuarios creados
     * Este método rellena el arreglo de información del usuario que inicia sesión
     * 
     */   
    public static void completarPerfil(ArrayList<Usuario> usuarios){
        int indice = usuarios.indexOf(Sesion.usuarioEscogido);
        Scanner info = new Scanner(System.in);
        String generos[] = {"Masculino","Femenino"};
        
        System.out.println("Por favor, proceda a completar los datos de su perfil.");

        System.out.println("\n------GÉNERO INTERESADO------");
        for (int i=0; i < generos.length; i++){
            System.out.println((i+1)+".) "+generos[i]);
        }
        System.out.print("Elija género: ");
        int dato1 = info.nextInt();
        
        while(dato1 <= 0 || dato1 > generos.length){
            System.out.println("Ingrese formato correcto.");
            System.out.print("Elija género: ");
            dato1 = info.nextInt();
        }
        usuarios.get(indice).getInformacion().add(0,generos[dato1-1]);
        info.nextLine();

        System.out.println("\n------EDAD DE INTERÉS------");
        String edadInteres = "0";
        while(Integer.parseInt(edadInteres) < 18){
            System.out.print("Ingrese la edad de su pareja ideal: ");
            edadInteres = info.nextLine();
            usuarios.get(indice).getInformacion().add(1,edadInteres);
        }

        System.out.println("\n------PROFESIÓN------");
        Profesiones profesiones[] = Usuarios.Profesiones.values();
        for (int i=0; i < profesiones.length; i++){
            System.out.println((i+1)+".) "+profesiones[i]);
        }
        System.out.print("Elija una profesión: ");
        int dato2 = info.nextInt();
        
        while(dato2 <= 0 || dato2 > profesiones.length){
            System.out.print("Elija una profesión: ");
            dato2 = info.nextInt();  
        }
        String profesion = valueOf(profesiones[dato2-1]);
        usuarios.get(indice).getInformacion().add(2,profesion);

        System.out.println("\n------HOBBIE------");
        Intereses intereses[] = Usuarios.Intereses.values();
        for (int i=0; i < intereses.length; i++){
            System.out.println((i+1)+".) "+intereses[i]);
        }
        System.out.print("Elija hobbie favorito: ");
        int dato3 = info.nextInt();
        
        while(dato3 <=0 || dato3 > intereses.length){
            System.out.println("Ingrese formato correcto");
            System.out.print("Elija hobbie favorito: ");
            dato3 = info.nextInt();            
        }
        String hobbie = valueOf(intereses[dato3-1]);
        usuarios.get(indice).getInformacion().add(3,hobbie);
 
        System.out.println("\n------MASCOTA------");
        Mascota mascotas[] = Usuarios.Mascota.values();
        for (int i=0; i < mascotas.length; i++){
            System.out.println((i+1)+".) "+mascotas[i]);
        }
        System.out.print("Elija mascota favorita: ");
        int dato4 = info.nextInt();
        
        while(dato4 <= 0 || dato4 > mascotas.length){
            System.out.println("Ingrese formato correcto.");
            System.out.print("Elija mascota favorita: ");
            dato4 = info.nextInt();
        }
        String mascota = valueOf(mascotas[dato4-1]);
        usuarios.get(indice).getInformacion().add(4,mascota);

        System.out.println("\n------GÉNERO MUSICAL------");
        Musica generosMusicales[] = Usuarios.Musica.values();
        for (int i=0; i < generosMusicales.length; i++){
            System.out.println((i+1)+".) "+generosMusicales[i]);
        } 
        System.out.print("Elija género musical: ");
        int dato5 = info.nextInt();
        
        while(dato5 <= 0 || dato5 > generosMusicales.length){
            System.out.println("Ingrese formato correcto.");
            System.out.print("Elija género musical: ");
            dato5 = info.nextInt();
        }
        String musica = valueOf(generosMusicales[dato5-1]);
        usuarios.get(indice).getInformacion().add(5,musica);
        
    }
    
    /**
     *
     * @author Guillermo Moreira
     * @param usuarios
     * Este método busca pareja para el usuario que haya iniciado sesión y muestra cada usuario registrado
     */  
    
    public static void buscarPareja(ArrayList<Usuario> usuarios){
        Scanner entrada = new Scanner(System.in);
        if (!Sesion.usuarioEscogido.getInformacion().isEmpty()){
            if (!usuarios.isEmpty()){
                System.out.println("\nUsuarios que te podrían interesar: ");
                for(int u=0; u < usuarios.size(); u++){
                    System.out.println("");
                    if(definirCompatibilidad(Sesion.usuarioEscogido, usuarios.get(u)) >= 50 && !usuarios.get(u).equals(Sesion.usuarioEscogido)){
                        String fecha = usuarios.get(u).getFechaNacimiento();
                        String[] f = fecha.split("/");
                        Integer anio = Integer.parseInt(f[2]);
                        System.out.print(usuarios.get(u).getNombre() + "\nEdad: " +(2020-anio)+"\nGénero de interés: " + usuarios.get(u).getInformacion().get(0) +"\nEdad de interés:  " +  usuarios.get(u).getInformacion().get(1)  + "\nProfesión: " + usuarios.get(u).getInformacion().get(2) +"\nIntereses: " + usuarios.get(u).getInformacion().get(3) + ", " + usuarios.get(u).getInformacion().get(4) + ", " + usuarios.get(u).getInformacion().get(5) + "\n");
                        System.out.println("\n1. Me gusta \n2. Siguiente \n3. Salir");  
                        System.out.print("Ingrese una opción: ");
                        String opcion = entrada.nextLine();
                        while(!opcion.equals("1") && !opcion.equals("2") && !opcion.equals("3")){
                            System.out.println("Ingrese formato correcto.");
                            System.out.print("Ingrese una opción: ");
                            opcion = entrada.nextLine();   
                        }
                        switch(opcion){
                            case "1":
                                if(Sesion.usuarioEscogido instanceof Standard){
                                    System.out.print("Ingrese mensaje: ");
                                    String texto = entrada.nextLine();
                                    Mensaje mensaje = new Mensaje(Sesion.usuarioEscogido,texto);
                                    Sesion.usuarioEscogido.enviarMensaje(usuarios.get(u),mensaje);
                                    System.out.print("Mensaje enviado!\nDesea continuar?(S/N): ");
                                    String decision = entrada.nextLine();
                                    String decisiones[] = {"S","N"};
                                    if(decision.equalsIgnoreCase(decisiones[0])){
                                        continue;
                                    }else if(decision.equalsIgnoreCase(decisiones[1])){
                                        break;
                                    }
                                }else if (Sesion.usuarioEscogido instanceof Premium){
                                    System.out.print("Desea dar súper like? (S/N): ");
                                    String resp = entrada.nextLine();
                                    String respuestas[] = {"S","N"};
                                    if (resp.equalsIgnoreCase(respuestas[0])){
                                        Premium user = (Premium) Sesion.usuarioEscogido;
                                        user.darSuperLike();
                                        System.out.print("Ingrese mensaje: ");
                                        String msg = entrada.nextLine();
                                        String texto = user.imprimirSuperLike() + "\n"+msg;
                                        Mensaje mensaje = new Mensaje(Sesion.usuarioEscogido, texto);
                                        Sesion.usuarioEscogido.enviarMensaje(usuarios.get(u),mensaje);
                                    } else if (resp.equalsIgnoreCase(respuestas[1])){
                                        System.out.print("Ingrese mensaje: ");
                                        String texto = entrada.nextLine();
                                        Mensaje mensaje = new Mensaje(Sesion.usuarioEscogido, texto);
                                        Sesion.usuarioEscogido.enviarMensaje(usuarios.get(u),mensaje);
                                    }
                                    System.out.print("Mensaje enviado!\nDesea continuar?(S/N): ");
                                    String decision = entrada.nextLine();
                                    String decisiones[] = {"S","N"};
                                    if(decision.equalsIgnoreCase(decisiones[0])){
                                        continue;
                                    }else if(decision.equalsIgnoreCase(decisiones[1])){
                                        break;
                                    }
                                }
                            case "2":
                                if (u == usuarios.size()){
                                break;
                                }
                            case "3":
                                break; 
                        }
                    }else{
                        System.out.println("No hay usuarios compatibles o se acabaron las opciones.");
                        break;
                    }
                }
            }else{
            System.out.println("No existen usuarios para ver.\n"); 
            }
        }else{
            System.out.println("Por favor, complete su perfil primero.");
        }     
    }
    
    /**
     *
     * @author Guillermo Moreira
     * @param u1
     * @param u2
     * @return porcentaje
     * Este metodo devuelve un numero evaluado entre 0 y 100 dependiendo del numero de conincidencias en intereses de los usuarios.
     */    
    
    public static double definirCompatibilidad(Usuario u1, Usuario u2){
        int contador = 0;
        String generoInteresu1 = u1.getInformacion().get(0);
        String generoInteresu2 = u2.getInformacion().get(0);
        
        String edadInteresu1 = u1.getInformacion().get(1);
        String edadInteresu2 = u2.getInformacion().get(1);
        
        String profesionu1 = u1.getInformacion().get(2);
        String profesionu2 = u2.getInformacion().get(2);
        
        String pasatiempoPreferidou1 = u1.getInformacion().get(3);
        String pasatiempoPreferidou2 = u2.getInformacion().get(3);
        
        String animalPreferidou1 = u1.getInformacion().get(4);
        String animalPreferidou2 = u2.getInformacion().get(4);
        
        String musicaPreferidau1 = u1.getInformacion().get(5);
        String musicaPreferidau2= u2.getInformacion().get(5);
        
        if(!generoInteresu1.equals(generoInteresu2)){
            contador ++;
        }
        if(edadInteresu1.equals(edadInteresu2)){
            contador ++;
        } 
        if(profesionu1.equals(profesionu2)){
            contador ++;
        }
        if(pasatiempoPreferidou1.equals(pasatiempoPreferidou2)){
            contador++;
        }
        if(animalPreferidou1.equals(animalPreferidou2)){
            contador ++;
        }
        if(musicaPreferidau1.equals(musicaPreferidau2)){
            contador ++;
        }
        
        double porcentaje = (contador*100)/6;
        return porcentaje;
    }
}